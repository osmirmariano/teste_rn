import 'react-native-gesture-handler';
import * as React from 'react';
import Routes from './Navigation'
import { NavigationContainer } from '@react-navigation/native';

const App = () =>{
    return (
        <NavigationContainer>
            <Routes/>
        </NavigationContainer>
    )

}
export default App
